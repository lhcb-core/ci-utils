This directory contains helpers and examples to set up Gitlab-CI jobs for LHCb
projects.

## Quick start

Add to your project a `.gitlab-ci.yml` like this:
```yml
include:
  - https://gitlab.cern.ch/lhcb-core/ci-utils/raw/master/python/init.yml
  - https://gitlab.cern.ch/lhcb-core/ci-utils/raw/master/python/py2.yml
```

## Data package deployment

To install a data package automatically onto CVMFS:

1. Add the deployment stage to your pipeline stages:

```
stages:
  - deploy_datapkg
```

2. Include the build and deploy job

```
include:
  - project: 'lhcb-core/ci-utils'
    ref: master
    file: '/data-pkg-deployment/build-and-deploy.yml'
```

3. Generate a secret following the [lbdevops](https://lbdevops.web.cern.ch/lbdevops/LbTaskRun.html#secret-management) documentation with namespace `datapkg` and key equal to the repository name (e.g. `WG/CharmWGProd`).

4. Add the `REQUEST_DEPLOY_URL_TOKEN` secret to [GitLab CI configuration](https://docs.gitlab.com/ee/ci/variables/#instance-level-cicd-environment-variables). It should be a **masked** and **protected** variable and all tags starting with `v` should be [marked as protected](https://docs.gitlab.com/ee/user/project/protected_tags.html).

More details about how data packages are deployed can be found in [lbdevops](https://lbdevops.web.cern.ch/lbdevops/LbTaskRun.html#data-package-deployment
).

**Note for PARAM data packages:** The project in which packages are installed can be overridden by setting the `DATA_PKG_PROJECT` variable in the GitLab CI configuration (similarly to the `REQUEST_DEPLOY_URL_TOKEN` variable).
If this variable is unset the value will default to `DBASE`.

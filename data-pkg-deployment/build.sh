#!/usr/bin/env bash
# Use bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

project=${1}
package=${2}
version=${3}
pipeline_id=${4}
echo "Building ${project} ${package} ${version}"

build_area=$(mktemp -d)
this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )
underscored_package=${package//\//_}
install_dir=${build_area}/${project}/${package}/${version}
src_zip_fn=${build_area}/${underscored_package}.${version}.lhcb-release.${pipeline_id}.src.zip

# Get the released sources of the data package
mkdir -p "${build_area}/${project}"
ln -s "/cvmfs/lhcb.cern.ch/lib/lhcb/${project}/cmt" "${build_area}/${project}/cmt"
git clone "https://gitlab.cern.ch/lhcb-datapkg/${package}.git" "${install_dir}" -b "${version}"
git -C "${install_dir}" lfs install --local
git -C "${install_dir}" lfs pull
rm -rf "${install_dir}/.git"

# Create the old style environment file
xenv_name="${install_dir}/${underscored_package}"
xenv_oldname="${xenv_name//.xenv/Environment.xml}"
if [ ! -f "$xenv_oldname" ]; then
    if [ -f "${install_dir}/WG_SemilepConfig.xenv" ]; then
        ln -s "${underscored_package}.xenv" "$xenv_oldname"
    fi
fi

# Build the package if required
if [ -f "${install_dir}/Makefile" ]; then
    echo "Run make"
    (cd "${install_dir}" &&
     export LHCBRELEASES=/cvmfs/lhcb.cern.ch/lib/lhcb &&
     make)
elif [ -f "${install_dir}/cmt/requirements" ]; then
    echo "Run CMT"
    (cd "${install_dir}/cmt" &&
     unset PWD &&
     unset CWD &&
     unset CMTSTRUCTURINGSTYLE &&
     export CMTPATH="${build_area}/${project}" &&
     export CMTCONFIG=x86_64-centos7-gcc9-opt &&
     /cvmfs/lhcb.cern.ch/lib/bin/Linux-x86_64/cmt config &&
     /cvmfs/lhcb.cern.ch/lib/bin/Linux-x86_64/cmt make)
else
    echo "No build required"
fi

# Create the zip file of sources
(cd "${build_area}" &&
 zip -r -q "${src_zip_fn}" "${project}/${package}/${version}")

# Generate the RPM spec file
"${this_dir}/generate_rpm_spec.py" "${project}" "${package}" "${version}" "${src_zip_fn}" "${PWD}"

# Build the RPM
rpmbuild -bb "${PWD}/rpmbuild/${underscored_package}.spec"

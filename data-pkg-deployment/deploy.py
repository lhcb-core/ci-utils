#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import time
import os

import requests
from requests_toolbelt import MultipartEncoder


def upload_rpm(pipeline_id, project, package, rpm_path):
    rpm_fn = os.path.basename(rpm_path)
    response = requests.get(
        "https://lhcb-core-tasks.web.cern.ch/hooks/datapkg"
        "/{pipeline_id}/{project}/{package}/{rpm_fn}/artifact_url/"
        .format(pipeline_id=pipeline_id, project=project, package=package, rpm_fn=rpm_fn),
        headers={"Authorization": "Bearer " + os.environ['REQUEST_DEPLOY_URL_TOKEN']},
        verify="/etc/pki/tls/certs/ca-bundle.crt",
    )
    response.raise_for_status()
    data = response.json()

    with open(rpm_path, "rb") as fp:
        # This is needed on Python 2 due to lack of dictionary ordering
        fields = list(data["fields"].items())
        fields += [("file", (rpm_fn, fp, 'application/octet-stream'))]
        m = MultipartEncoder(fields=fields)
        response = requests.post(data["url"], data=m, headers={'content-type': m.content_type}, verify="/etc/pki/tls/certs/ca-bundle.crt")
    response.raise_for_status()


def deploy_rpm(pipeline_id, project, package, rpm_path):
    rpm_fn = os.path.basename(rpm_path)
    response = requests.put(
        "https://lhcb-core-tasks.web.cern.ch/hooks/datapkg"
        "/{pipeline_id}/{project}/{package}/{rpm_fn}/deploy/"
        .format(pipeline_id=pipeline_id, project=project, package=package, rpm_fn=rpm_fn),
        headers={"Authorization": "Bearer " + os.environ['REQUEST_DEPLOY_URL_TOKEN']},
        verify="/etc/pki/tls/certs/ca-bundle.crt",
    )
    response.raise_for_status()
    message_id = response.json()
    i = 0
    while True:
        response = requests.get("https://lhcb-core-tasks.web.cern.ch/tasks/status/" + message_id, verify="/etc/pki/tls/certs/ca-bundle.crt")
        if response.status_code == 200:
            status = response.json()
            if status == "SUCCESS":
                print("Deployment succeeded")
                return
            elif status == "FAILURE":
                raise Exception(
                    "Deployment failed, for details see:\n"
                    "https://lhcb-core-tasks.web.cern.ch/tasks/result/" + message_id
                )
            else:
                print("Status is", status)
        elif response.status_code == 404:
            print("Task does not appear to have started yet")
        else:
            print("Unrecognised HTTP status code", response.status_code)
        time.sleep(15)
        i += 1
        if i > 4*20:
            print(
                "If this persists for long enough for the job to fail, "
                "open an ticket at https://its.cern.ch/jira/projects/LHCBDEP/"
            )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("pipeline_id")
    parser.add_argument("project")
    parser.add_argument("package")
    parser.add_argument("rpm_path")
    args = parser.parse_args()

    upload_rpm(args.pipeline_id, args.project, args.package, args.rpm_path)
    deploy_rpm(args.pipeline_id, args.project, args.package, args.rpm_path)


if __name__ == "__main__":
    main()
